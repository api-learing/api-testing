package com.api.test.get;

import org.testng.annotations.Test;

import com.mastercard.api.GetRequest;
import com.mastercard.api.pojos.GetCityWeatherPojo;

import io.restassured.response.Response;

public class SimpleGetTest {

	@Test
	public void test01_get() {

		String uri = "http://restapi.demoqa.com/utilities/weather/city/";

		GetRequest request = new GetRequest();
		request.setUri(uri);

		Response response = request.getResponse("Hyderabad");

		System.out.println("Status code : "+response.getStatusCode());
		System.out.println("Status Line : "+response.getStatusLine());
		System.out.println("Response Body : "+response.getBody().asString());

		GetCityWeatherPojo g = response.getBody().as(GetCityWeatherPojo.class);
		System.out.println("\n\nResponse Body From Object : " + g);
		
		System.out.println("City from Json Body : " + response.jsonPath().get("City"));

		System.out.println("All Headers : " + response.getHeaders());
		

	}

}
