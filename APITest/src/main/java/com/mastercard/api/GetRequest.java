package com.mastercard.api;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetRequest {

	private RequestSpecification httpRequest = null;
	private Response response = null;

	public void setUri(String uri) {
		RestAssured.baseURI = uri;
	}

	public void initializeRequest() {
		httpRequest = RestAssured.given();
	}

	public Response getResponse(String cityName) {
		if (httpRequest == null) {
			initializeRequest();
		}
		return response = httpRequest.request(Method.GET, cityName);
	}
}
