package com.mastercard.api.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class GetCityWeatherPojo {

	@JsonProperty("City")
	private String City;

	@JsonProperty("Temperature")
	private String Temperature;

	@JsonProperty("Humidity")
	private String Humidity;

	@JsonProperty("WeatherDescription")
	private String WeatherDescription;

	@JsonProperty("WindSpeed")
	private String WindSpeed;

	@JsonProperty("WindDirectionDegree")
	private String WindDirectionDegree;

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getTemperature() {
		return Temperature;
	}

	public void setTemperature(String temperature) {
		Temperature = temperature;
	}

	public String getHumidity() {
		return Humidity;
	}

	public void setHumidity(String humidity) {
		Humidity = humidity;
	}

	public String getWeatherDescription() {
		return WeatherDescription;
	}

	public void setWeatherDescription(String weatherDescription) {
		WeatherDescription = weatherDescription;
	}

	public String getWindSpeed() {
		return WindSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		WindSpeed = windSpeed;
	}

	public String getWindDirectionDegree() {
		return WindDirectionDegree;
	}

	public void setWindDirectionDegree(String windDirectionDegree) {
		WindDirectionDegree = windDirectionDegree;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((City == null) ? 0 : City.hashCode());
		result = prime * result + ((Humidity == null) ? 0 : Humidity.hashCode());
		result = prime * result + ((Temperature == null) ? 0 : Temperature.hashCode());
		result = prime * result + ((WeatherDescription == null) ? 0 : WeatherDescription.hashCode());
		result = prime * result + ((WindDirectionDegree == null) ? 0 : WindDirectionDegree.hashCode());
		result = prime * result + ((WindSpeed == null) ? 0 : WindSpeed.hashCode());
		return result;
	}

	
	
	@Override
	public String toString() {
		return "GetCityWeatherPojo [City=" + City + ", Temperature=" + Temperature + ", Humidity=" + Humidity
				+ ", WeatherDescription=" + WeatherDescription + ", WindSpeed=" + WindSpeed + ", WindDirectionDegree="
				+ WindDirectionDegree + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetCityWeatherPojo other = (GetCityWeatherPojo) obj;
		if (City == null) {
			if (other.City != null)
				return false;
		} else if (!City.equals(other.City))
			return false;
		if (Humidity == null) {
			if (other.Humidity != null)
				return false;
		} else if (!Humidity.equals(other.Humidity))
			return false;
		if (Temperature == null) {
			if (other.Temperature != null)
				return false;
		} else if (!Temperature.equals(other.Temperature))
			return false;
		if (WeatherDescription == null) {
			if (other.WeatherDescription != null)
				return false;
		} else if (!WeatherDescription.equals(other.WeatherDescription))
			return false;
		if (WindDirectionDegree == null) {
			if (other.WindDirectionDegree != null)
				return false;
		} else if (!WindDirectionDegree.equals(other.WindDirectionDegree))
			return false;
		if (WindSpeed == null) {
			if (other.WindSpeed != null)
				return false;
		} else if (!WindSpeed.equals(other.WindSpeed))
			return false;
		return true;
	}

	
	
}
